var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanaliGeslo = {};

function vsiUporabnikiNaKanalu(kanal) {
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  var uporabnikiNaKanaluPovzetek = [];
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    uporabnikiNaKanaluPovzetek.push(vzdevkiGledeNaSocket[uporabnikSocketId]);
  }

  return uporabnikiNaKanaluPovzetek;
}

exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function(socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function(kanal) {
      socket.emit('uporabniki', {
        vzdevki: vsiUporabnikiNaKanalu(kanal.imeKanala)
      });
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function najdiUporabnikovSocket(vzdevek, vzdevkiGledeNaSocket) {
  for (var i in vzdevkiGledeNaSocket) {
    if (vzdevkiGledeNaSocket[i] == vzdevek) {
      return i;
    }
  }
}

function obdelajZasebnoSporocilo(socket, vzdevkiGledeNaSocket) {
  socket.on('zasebno', function(sporocilo) {
    var besedilo = "Sporočila '" + sporocilo.besedilo + "' uporabniku z vzdevkom '" + sporocilo.vzdevek + "' ni bilo mozno posredovati.";
    if (uporabljeniVzdevki.indexOf(sporocilo.vzdevek) == -1) {
      socket.emit('sporociloSys', {
        besedilo: besedilo,
      });
    }
    else {
      var soc = najdiUporabnikovSocket(sporocilo.vzdevek, vzdevkiGledeNaSocket);
      if (soc != socket.id) {
        besedilo = '(zasebno): ' + sporocilo.besedilo;
        io.sockets.socket(soc).emit('sporocilo', {
          besedilo: besedilo,
          vzdevek: vzdevkiGledeNaSocket[socket.id]
        });
      }
      else {
        socket.emit('sporociloSys', {
          besedilo: besedilo
        });
      }

    }
  });
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek,
    kanal: trenutniKanal[socket.id]
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function aliJeKdoNaKanalu(kanal) {
  for (var k in trenutniKanal) {
    if (trenutniKanal[k] == kanal) {
      return true;
    }
  }
  return false;
}

function pridruzitevKanalu(socket, kanal, geslo) {
  if ((kanaliGeslo.hasOwnProperty(kanal) && geslo == kanaliGeslo[kanal]) || !kanaliGeslo.hasOwnProperty(kanal)) {
    if (!kanaliGeslo.hasOwnProperty(kanal)) {
      kanaliGeslo[kanal] = geslo;
    }
    var prejsnjiKanal = trenutniKanal[socket.id];
    socket.leave(trenutniKanal[socket.id]);
    socket.join(kanal);
    trenutniKanal[socket.id] = kanal;
    if (!aliJeKdoNaKanalu(prejsnjiKanal)) {
      delete kanaliGeslo[prejsnjiKanal];
    }
    socket.emit('pridruzitevOdgovor', {
      kanal: kanal,
      vzdevek: vzdevkiGledeNaSocket[socket.id]
    });
    socket.broadcast.to(kanal).emit('sporociloSys', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    });

    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
      uporabnikiNaKanaluPovzetek += '.';
      socket.emit('sporociloSys', {
        besedilo: uporabnikiNaKanaluPovzetek
      });
    }
  }
  else {
    var sporocilo;
    if (kanaliGeslo.hasOwnProperty(kanal) && kanaliGeslo[kanal] == null && geslo != null) {
      sporocilo = "Izbrani kanal '" + kanal + "' je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev &lt;kanal&gt; ali zahtevajte kreiranje kanala z drugim imenom";
    }
    else {
      sporocilo = "Pridružitev na kanal '" + kanal + "' ni bilo uspešno, ker je geslo napačno!";
    }

    socket.emit('sporociloSys', {
      besedilo: sporocilo
    });
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".',
        vzdevek: vzdevek
      });
    }
    else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporociloSys', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      }
      else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function(sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo', {
      besedilo: ': ' + sporocilo.besedilo,
      vzdevek: vzdevkiGledeNaSocket[socket.id]
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
  });
}


function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}