var smiley = ['[;][)]', '[:][)]', '[(][y][)]', '[:][*]', '[:][(]'];
var smiley_links = ['https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png',
  'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png',
  'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png',
  'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png',
  'https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'
];
var swearWords = $('<div>').load("../swearWords.txt", function() {
  swearWords = swearWords.text().split("\n");
});

function antiXSS(sporocilo) {
  return sporocilo.replace(new RegExp("<", "g"), '&lt');
}

function obdelajSporocilo(sporocilo) {
  sporocilo = antiXSS(sporocilo);
  for (var i = 0; i < smiley.length; i++) {
    sporocilo = sporocilo.replace(new RegExp(smiley[i], "g"), '<img src="' + smiley_links[i] + '"/>');
  }
  sporocilo = filterSwearWords(sporocilo);
  return sporocilo;
}

function filter(i) {
  return new Array(swearWords[i].length + 1).join('*');
}

function filterSwearWords(sporocilo) {
  for (var i = 0; i < swearWords.length; i++) {
    sporocilo = sporocilo.replace(new RegExp("\\b" + swearWords[i] + "\\b", "gi"), filter(i));
    sporocilo = sporocilo.replace(new RegExp("&lt" + swearWords[i] + "\\b", "gi"), "&lt" + filter(i));
  }
  return sporocilo;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  sporocilo = antiXSS(sporocilo);
  return $('<div style="font-weight: italics"></div>').html(sporocilo);
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  }
  else {
    var kanal = $('#kanal').text().split("@")[1].substring(1);
    klepetApp.posljiSporocilo(kanal, {
      sporocilo: sporocilo
    });
    sporocilo = obdelajSporocilo(sporocilo);
    $('#sporocila').append($('<div style="font-weight: bold"></div>').html(sporocilo));

    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);

    }
    else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function(sporocilo) {
    sporocilo.besedilo = antiXSS(sporocilo.vzdevek) + obdelajSporocilo(sporocilo.besedilo);
    $('#sporocila').append($('<div style="font-weight: bold"></div>').html(sporocilo.besedilo));
  });

  socket.on('sporociloSys', function(sporocilo) {
    $('#sporocila').append(divElementHtmlTekst(sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for (var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('uporabniki', function(users) {
    $('#uporabniki').empty();
    var uporabniki = users.vzdevki;
    for (var i = 0; i < uporabniki.length; i++) {
      $('#uporabniki').append(divElementEnostavniTekst(uporabniki[i]));
    }

    $('#uporabniki div').click(function() {
      $('#poslji-sporocilo').val('/zasebno "' + $(this).text() + '"' + ' "[sporočilo]"');
      document.getElementById("poslji-sporocilo").setSelectionRange($('#poslji-sporocilo').val().lastIndexOf('['), $('#poslji-sporocilo').val().length - 1);
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  setInterval(function() {
    socket.emit('uporabniki', {
      imeKanala: $('#kanal').text().split("@")[1].substring(1)
    });
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});