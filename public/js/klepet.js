var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo.sporocilo,
    html: besedilo.html
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;
  switch (ukaz) {
    case 'pridruzitev':
      besede.shift();
      besede = besede.join(' ').split('"');
      if (besede.length == 5 && /^ *$/.test(besede[0]) && /^ *$/.test(besede[2]) && /^ *$/.test(besede[4])) {
        this.spremeniKanal(besede[1], besede[3]);
      }
      else {
        var kanal = besede.join('"');
        this.spremeniKanal(kanal, null);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      besede = besede.join(' ').split('"');
      if (besede.length == 5 && /^ *$/.test(besede[0]) && /^ *$/.test(besede[2]) && /^ *$/.test(besede[4])) {
        this.socket.emit('zasebno', {
          vzdevek: besede[1],
          besedilo: besede[3]
        });
        besede[3] = obdelajSporocilo(besede[3]);
        besede[1] = antiXSS(besede[1]);
        besede[3] = "(zasebno za " + besede[1] + "): " + besede[3];
        $('#sporocila').append($('<div style="font-weight: bold"></div>').html(besede[3]));
        break;
      }
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};